package com.mycompany.app;

import com.javabycode.springboot.MyWebApplication;

/**
 * Hello world!
 */
public class App
{

    private final String message = "Hello World!";

    public App() {}

    public static void main(String[] args) {


        System.out.println(new App().getMessage());
    }

    private final String getMessage() {
        return message;
    }

}
